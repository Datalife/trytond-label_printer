# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .printer import PrinterType, Printer


def register():
    Pool.register(
        PrinterType,
        Printer,
        module='label_printer', type_='model')
